package facci.pm.pruebaunitaria;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import android.provider.ContactsContract;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@LargeTest
@RunWith(AndroidJUnit4ClassRunner.class)
public class MainActivityTest {

    public static final String STRING_TO_BE_TYPED = "34";


    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);


    @Test
    public void myFirstTest() {
        onView(withId(R.id.editTextCorreo)).perform(typeText(STRING_TO_BE_TYPED),closeSoftKeyboard());
        onView(withId(R.id.editTextCorreo)).check(matches(withText(STRING_TO_BE_TYPED)));
    }

    @Test
    public void mySecondTest() {
        onView(withId(R.id.editTextPass)).perform(typeText(STRING_TO_BE_TYPED),closeSoftKeyboard());
        onView(withId(R.id.editTextPass)).check(matches(withText(STRING_TO_BE_TYPED)));
    }
    @Test
    public void myThirdTest() {
        onView(withId(R.id.editTextCorreo)).perform(typeText(STRING_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.editTextPass)).perform(typeText(STRING_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.btnLogin)).perform(click());
    }

}
