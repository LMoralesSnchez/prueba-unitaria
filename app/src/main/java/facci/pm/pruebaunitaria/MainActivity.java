package facci.pm.pruebaunitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etCorreo,etPass;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();
    }
    private void setViews() {
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        etCorreo = findViewById(R.id.editTextCorreo);
        etPass = findViewById(R.id.editTextPass);
    }
//    private boolean hasMailSucces() {
//        return !etCorreo.getText().toString().isEmpty();
//    }
//    private boolean hasPassSucces() {
//        return !etPass.getText().toString().isEmpty();
//    }

    @Override
    public void onClick(View v) {
        if (v == btnLogin) {
            if (validar(etCorreo.getText().toString(), etPass.getText().toString())) {
                Toast.makeText(this, R.string.logeado, Toast.LENGTH_SHORT).show();
            } else {
                Log.e("test", "Wanting not Welcome");
            }
        }
    }

    public boolean validar(String mail, String pass) {

        if (Check.validateEmpty(mail) || Check.validateEmpty(pass)) {
            Toast.makeText(this, R.string.nocredenciales, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Check.validateMail(mail)) {
            Toast.makeText(this, R.string.nomail, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Check.validateUser(mail, pass)) {
            Toast.makeText(this, R.string.nologeado, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}