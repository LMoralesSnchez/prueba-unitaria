package facci.pm.pruebaunitaria;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CheckTest {
    @Test
    public void isEmailFormatCorrect_ReturnsFalse() {
        assertFalse(Check.validateMail("luis"));
    }
    @Test
    public void isEmailFormatCorrect_ReturnTrue() {
        assertTrue(Check.validateMail("luis@admin.com"));
    }
    @Test
    public void isUserinBD_ReturnTrue() {
        assertTrue(Check.validateUser("luis@admin.com","asd123"));
    }
    @Test
    public void isUserinBD_ReturnFalse() {
        assertFalse(Check.validateUser("x@admin.com","asd123"));
    }
}
